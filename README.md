# WorkWorkStats [![pipeline status](https://gitlab.com/ddikman/work-work-stats/badges/master/pipeline.svg)](https://gitlab.com/ddikman/work-work-stats/commits/master) [![coverage report](https://gitlab.com/ddikman/work-work-stats/badges/master/coverage.svg)](https://gitlab.com/ddikman/work-work-stats/commits/master)
Work Work Stats is a simple tool for pulling out your team's performance based on github commits.


## What is a good PR?

I just keep this a good reference [The anatomy of a perfect pull request](https://medium.com/@hugooodias/the-anatomy-of-a-perfect-pull-request-567382bb6067).

## Get started

### 1. Install prereqs

The project is written using Angular 7 which means you'll have to have Node Js and the Angular CLI installed in order to run it.

**for osx**
It's preferred to install the [Node Version manager](https://github.com/nvm-sh/nvm). Then install `10.16.0`.

```sh
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
nvm install 10.16.0
node --version
```

**for windows**

Run the [installer for Node JS 10.16.0](https://nodejs.org/dist/v10.16.0/node-v10.16.0-x86.msi).

**both**
After installing node, also install the Angular CLI. This can be done in any folder since it's installed globally:

```sh
npm install --global @angular/cli
```

Also, if you don't already have it, install git ([for windows](https://git-scm.com/download/win)|[for osx](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)).

### 2. Clone the repo
Navigate to any nice directory on your folder and run the following, it will create the folder you  can then `cd` into.

```sh
git clone https://gitlab.com/greycastle/work-work-stats.git
cd work-work-stats
```

### 3. Install packages
Install all the required packages. Go make coffee, this will take a while.

```sh
npm install
```

### 4. Add secrets
You'll notice that there is a file called `src/environments/environment.ts` which has some placeholder values in it. This is to avoid checking in secrets to the repo. You can find these secrets in firebase launch portal in instructions on how to set up or simply ask the repo owner. They can also provide you with a test PAT (personal access token) for when you're running the tests.

Copy the `environment.ts` and add in an `environment.dev.ts` in the same folder with the configuration values specified.

### 5. Serve
Run the application:

### 4. Serve
```bash
npm start
```

### 5. Enjoy!
You can now access it on [http://localhost:4200](http://localhost:4200).
