const environment = `
export const environment = {
    production: false,
    firebase: {
        apiKey: '${process.env.FIREBASE_APIKEY}',
        authDomain: '${process.env.FIREBASE_AUTHDOMAIN}',
        databaseURL: '${process.env.FIREBASE_DATABASEURL}',
        projectId: '${process.env.FIREBASE_PROJECTID}'
    },
    testToken: '${process.env.TEST_TOKEN}'
};`;

process.stdout.write(environment);
