import { debounceTime } from 'rxjs/operators';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTeamRepoButtonComponent } from './add-team-repo-button.component';
import { By } from '@angular/platform-browser';
import { TeamService } from '../team.service';

describe('AddTeamRepoButtonComponent', () => {
  let component: AddTeamRepoButtonComponent;
  let fixture: ComponentFixture<AddTeamRepoButtonComponent>;
  let teamService: TeamService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTeamRepoButtonComponent ],
      providers: [ TeamService ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(AddTeamRepoButtonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    teamService = TestBed.get(TeamService);
    teamService.reset();
  });

  it('should add repo to list after save', () => {
    // given the repo name
    component.repositoryName = 'test repository';

    // when pressing the button
    const button = fixture.debugElement.query(By.css('.ui.button'));
    button.triggerEventHandler('click', null);

    // we should have saved the repo to the list
    expect(teamService.repositories).toContain(component.repositoryName);
  });
});
