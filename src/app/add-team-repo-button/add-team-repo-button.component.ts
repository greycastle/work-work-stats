import { TeamService } from './../team.service';
import { ToastService } from './../toast.service';
import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-add-team-repo-button',
  templateUrl: './add-team-repo-button.component.html',
  styleUrls: ['./add-team-repo-button.component.sass']
})
export class AddTeamRepoButtonComponent {
  @Input()
  repositoryName: string;

  constructor(private toast: ToastService, private teamService: TeamService) { }

  addToTeam() {
    this.teamService.addRepository(this.repositoryName);
    this.toast.show('Added!', `Added ${this.repositoryName} to the current team.`);
  }
}
