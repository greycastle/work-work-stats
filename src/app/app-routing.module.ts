import { TokenGuard } from './token.guard';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { OrganisationComponent } from './organisation/organisation.component';
import { RepositoryComponent } from './repository/repository.component';
import { TeamStatsComponent } from './team-stats/team-stats.component';

const canActivate = [ TokenGuard ];

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'org/:org', component: OrganisationComponent, canActivate },
  { path: 'org/:org/repo/:repo', component: RepositoryComponent, canActivate },
  { path: 'team-stats', component: TeamStatsComponent, canActivate }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
