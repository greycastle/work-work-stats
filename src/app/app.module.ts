import { RepoSearchServiceToken } from './repo-search-service';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { HttpClientModule } from '@angular/common/http';
import { ReactiveFormsModule } from '@angular/forms';

import { AngularFireModule  } from 'angularfire2';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { AngularFirestoreModule } from 'angularfire2/firestore';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { OrganisationComponent } from './organisation/organisation.component';
import { RepositoryComponent } from './repository/repository.component';
import { SearchRepositoryComponent } from './search-repository/search-repository.component';
import { GithubRepoSearchService } from './github-repo-search.service';
import { GithubTokenProvider } from './github-token-provider';
import { BackComponent } from './back/back.component';
import { AddTeamRepoButtonComponent } from './add-team-repo-button/add-team-repo-button.component';
import { TeamStatsComponent } from './team-stats/team-stats.component';
import { environment } from 'src/environments/environment';
import { ChangeRatePipe } from './change-rate.pipe';
import { PullRequestStatsComponent } from './pull-request-stats/pull-request-stats.component';
import { PullRequestLifetimeStatsComponent, OldestPullRequestLifetimeStatsComponent } from './pull-request-lifetime-stats/pull-request-lifetime-stats.component';
import { HumanizePipe } from './humanize.pipe';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    OrganisationComponent,
    RepositoryComponent,
    SearchRepositoryComponent,
    BackComponent,
    AddTeamRepoButtonComponent,
    PullRequestStatsComponent,
    ChangeRatePipe,
    TeamStatsComponent,
    PullRequestLifetimeStatsComponent,
    OldestPullRequestLifetimeStatsComponent,
    HumanizePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireAuthModule,
    AngularFirestoreModule
  ],
  providers: [
    { provide: RepoSearchServiceToken, useClass: GithubRepoSearchService },
    GithubTokenProvider
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
