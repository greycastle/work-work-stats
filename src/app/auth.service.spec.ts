import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { AngularFireAuth } from 'angularfire2/auth';
import { GithubTokenProvider } from './github-token-provider';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable, of } from 'rxjs';

describe('AuthService', () => {
  const tokenProvider = new GithubTokenProvider();

  const authState = {
    displayName: null,
    isAnonymous: true,
    uid: '17WvU2Vj58SnTz8v7EqyYYb0WRc2'
  };


  const mockAngularFireAuth: any = {
    auth: jasmine.createSpyObj('auth', {
      signInWithPopup: Promise.reject('error')
    }),
    authState: of(authState)
  };

  let firestore: jasmine.SpyObj<AngularFirestore>;
  let service: AuthService;

  beforeEach(() => {
    firestore = jasmine.createSpyObj<AngularFirestore>(['doc']);
    service = new AuthService(mockAngularFireAuth, tokenProvider, firestore);
  });

  it('should be created', async () => {
    try {
      await service.githubLogin();
    } catch (err) {
      expect(err).toBe('error');
    }
  });
});
