import { GithubTokenProvider } from './github-token-provider';
import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { AngularFirestore } from 'angularfire2/firestore';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(public afAuth: AngularFireAuth, private tokenProvider: GithubTokenProvider, private firestore: AngularFirestore) { }

  githubLogin(): Promise<string> {
    return new Promise<any>((resolve, reject) => {
      const provider = new firebase.auth.GithubAuthProvider();
      provider.addScope('read:org');
      provider.addScope('repo');
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        console.log(res);
        this.firestore.doc(`user-data/${res.user.uid}`).set({
          lastLogin: new Date()
        });
        const token = (res.credential as any).accessToken;
        this.tokenProvider.token = token;
        resolve(token);
      }, err => {
        console.log(err);
        reject(err);
      });
    });
 }
}
