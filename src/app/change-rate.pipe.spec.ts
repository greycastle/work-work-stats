import { ChangeRatePipe } from './change-rate.pipe';

describe('ChangeRatePipe', () => {
  const directive = new ChangeRatePipe();

  it('should prefix with plus on positive number', () => {
    expect(directive.transform(1)).toBe('+1');
  });

  it('should prefix with minus on negative number', () => {
    expect(directive.transform(-1)).toBe('-1');
  });

  it('should be plus/minus zero on no change', () => {
    expect(directive.transform(0)).toBe('±0');
  });

  it('should work on floats', () => {
    expect(directive.transform(44.4)).toBe('+44.4');
  });
});
