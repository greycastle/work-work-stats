import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'changeRate'
})
export class ChangeRatePipe implements PipeTransform {
  transform(value: any, ...args: any[]) {
    if (value > 0) {
      return `+${value}`;
    } else if (value < 0) {
      return value.toString();
    }
    return `±${value}`;
  }
}
