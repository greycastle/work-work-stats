import { map } from 'rxjs/operators';
import { GithubApiClientService } from './github-api-client.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ContributorStatsService {
  constructor(private githubApiClient: GithubApiClientService) {}

  private getStats(stats: any[]): any {
    const contributors: any[] = stats.map(contributor => ({
      name: contributor.author.login,
      url: contributor.author.html_url,
      additions: contributor.weeks.reduce((sum, week) => sum + week.a, 0),
      removals: contributor.weeks.reduce((sum, week) => sum + week.d, 0),
      commits: contributor.weeks.reduce((sum, week) => sum + week.c, 0)
    }));

    const totals = {
      contributors,
      adds: contributors.reduce((sum, contributor) => sum + contributor.additions, 0),
      deletes: contributors.reduce((sum, contributor) => sum + contributor.removals, 0),
      commits: contributors.reduce((sum, contributor) => sum + contributor.commits, 0),
      addsPerCommit: null,
      deletesPerCommit: null
    };

    totals.addsPerCommit = Math.round(totals.adds / totals.commits);
    totals.deletesPerCommit = Math.round(totals.deletes / totals.commits);

    contributors.forEach(c => {
      c.percentage = {
          additions: Math.round((c.additions / totals.adds) * 100),
          deletes: Math.round((c.removals / totals.deletes) * 100),
          commits: Math.round((c.commits / totals.commits) * 100),
      };
    });

    return totals;
  }

  private combine(allStats: any[], contributors: string[]): any {

    const contributorStats = contributors.map(name => {
      const contributions = allStats.map(stats => stats.contributors.find(c => c.name === name)).filter(item => item !== undefined);
      return {
        name,
        url: contributions[0].url,
        additions: contributions.reduce((sum, c) => sum += c.additions, 0),
        removals: contributions.reduce((sum, c) => sum += c.removals, 0),
        commits: contributions.reduce((sum, c) => sum += c.commits, 0),
        percentage: {}
      };
    });

    const totals = {
      contributors: contributorStats,
      adds: allStats.reduce((sum, stats) => sum += stats.adds, 0),
      deletes: allStats.reduce((sum, stats) => sum += stats.deletes, 0),
      commits: allStats.reduce((sum, stats) => sum += stats.commits, 0),
      addsPerCommit: null,
      deletesPerCommit: null
    };

    totals.addsPerCommit = Math.round(totals.adds / totals.commits);
    totals.deletesPerCommit = Math.round(totals.deletes / totals.commits);

    contributorStats.forEach(c => {
      c.percentage = {
          additions: Math.round((c.additions / totals.adds) * 100),
          deletes: Math.round((c.removals / totals.deletes) * 100),
          commits: Math.round((c.commits / totals.commits) * 100),
      };
    });

    return totals;
  }

  private async getStatsForContributors(org: string, repo: string, filter: string[]) {
    let contributors: any = {};

    // while the stats are loading, the contributor list will be empty so retry until we get something
    let retries = 10;
    while (retries > 0  && !contributors.length) {
      contributors = await this.githubApiClient.get(`/repos/${org}/${repo}/stats/contributors`).toPromise<any>();
      retries -= 1;
    }

    // because this happens for some reason
    if (contributors.length === null) {
      throw Error('Failed to load contributors');
    }
    if (filter.length > 0) {
      contributors = contributors.filter(c => filter.find(name => name === c.author.login));
    }
    return this.getStats(contributors);
  }

  async get(org: string, repo: string): Promise<any> {
    return this.getStatsForContributors(org, repo, []);
  }

  async getTotals(org: string, repositories: string[], contributors: string[]) {
    const allStats = [];
    for (const repo of repositories) {
      const stats = await this.getStatsForContributors(org, repo, contributors);
      allStats.push(stats);
    }

    return this.combine(allStats, contributors);
  }
}
