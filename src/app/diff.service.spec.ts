import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { GithubApiClientService } from './github-api-client.service';
import { GithubTokenProvider } from './github-token-provider';
import { environment } from 'src/environments/environment.dev';
import { DiffService } from './diff.service';
import { Observable } from 'rxjs';

class CountableApiService extends GithubApiClientService {
  serviceCalls = 0;

  getDiff(url: string): Observable<string> {
    this.serviceCalls += 1;
    return super.getDiff(url);
  }
}

describe('DiffService', () => {
  let service: DiffService;
  let apiClient: CountableApiService;

  const anExamplePr = {
    number: 30919,
    repository: 'flutter',
    organisation: 'flutter'
  };

  beforeEach(async () => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [
        { provide: GithubApiClientService, useClass: CountableApiService },
        GithubTokenProvider,
        DiffService
      ]
    });

    const tokenProvider: GithubTokenProvider = TestBed.get(GithubTokenProvider);
    tokenProvider.token = environment.testToken;
    apiClient = TestBed.get(GithubApiClientService);;

    service = TestBed.get(DiffService);
  });

  it('can get number of differences', async () => {
    // given
    const pr = anExamplePr;

    // when getting the differences
    const result = await service.getDifferences(pr.number, pr.repository, pr.organisation);

    // then
    expect(result.additions).toBe(3);
    expect(result.deletions).toBe(2);
  });

  it('caches diffs in memory and calls the api only once per pr', async () => {
    // given
    const pr = anExamplePr;

    // when getting the differences twice
    await service.getDifferences(pr.number, pr.repository, pr.organisation);
    await service.getDifferences(pr.number, pr.repository, pr.organisation);

    // the api is only called once
    expect(apiClient.serviceCalls).toBe(1);
  });
});
