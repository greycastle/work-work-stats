import { Injectable } from '@angular/core';
import { GithubApiClientService } from './github-api-client.service';

export class Differences {
  readonly additions: number;
  readonly deletions: number;

  get total(): number {
    return this.additions + this.deletions;
  }

  constructor(additions: number, deletions: number) {
    this.additions = additions;
    this.deletions = deletions;
  }
}

@Injectable({
  providedIn: 'root'
})
export class DiffService {
  private readonly cache = new Map<string, Differences>();

  constructor(private githubService: GithubApiClientService) { }

  private static calculate(diffText: string): Differences {
    const additionsRegex = /^\+[^\+]/gm;
    const deletionsRegex = /^\-[^\-]/gm;
    const additions = (diffText.match(additionsRegex) || []).length;
    const deletions = (diffText.match(deletionsRegex) || []).length;
    return new Differences(additions, deletions);
  }

  getDifferences(prNumber: number, repository: string, organisation: string): Promise<Differences> {
    const url = `repos/${organisation}/${repository}/pulls/${prNumber}`;

    if (this.cache.has(url)) {
      return Promise.resolve(this.cache[url]);
    }

    return new Promise((resolve, reject) => {
      this.githubService.getDiff(url)
      .subscribe(result => {
        const difference = DiffService.calculate(result.toString());
        this.cache.set(url, difference);
        resolve(difference);
      }, err => reject(err));
    });
  }
}
