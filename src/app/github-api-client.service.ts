import { HttpClient, HttpHeaders, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GithubTokenProvider } from './github-token-provider';

@Injectable({
  providedIn: 'root'
})
export class GithubApiClientService {

  constructor(private tokenProvider: GithubTokenProvider, private httpClient: HttpClient) { }

  private readonly baseUrl = 'https://api.github.com';
  static prefixSlash(url: string): string {
    return url.startsWith('/') ? url : ('/' + url);
  }

  getDiff(url: string): Observable<string> {
    const absoluteUrl: string = this.baseUrl + GithubApiClientService.prefixSlash(url);

    const headers = new HttpHeaders({
      Accept: 'application/vnd.github.v3.diff',
      Authorization: `token ${this.tokenProvider.token}`
    });

    return this.httpClient.get(absoluteUrl, { headers, responseType: 'text' });
  }

  get(url: string): Observable<any> {
    const absoluteUrl = this.baseUrl + GithubApiClientService.prefixSlash(url);

    const headers = new HttpHeaders({
      Accept: 'application/vnd.github.antiope-preview+json',
      Authorization: `token ${this.tokenProvider.token}`
    });

    return this.httpClient.get<any>(absoluteUrl, { headers });
  }
}
