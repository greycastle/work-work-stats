import { GithubApiClientService } from './github-api-client.service';
import { Injectable } from '@angular/core';
import { RepoSearchService } from './repo-search-service';
import { Observable } from 'rxjs';
import { RepositorySearchResultEntry } from './repository-search-result-entry';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GithubRepoSearchService implements RepoSearchService {

  constructor(private apiClient: GithubApiClientService) { }

  search(organisation: string, filter: string): Observable<RepositorySearchResultEntry[]> {
    const encodedQuery = encodeURI(`${filter}+org:${organisation}`);
    const url = `/search/repositories?q=${encodedQuery}`;
    return this.apiClient.get(url)
      .pipe(map((result: any) => this.map(result)));
  }
  map(result: any): RepositorySearchResultEntry[] {
    return result.items.map((item: any) => RepositorySearchResultEntry.build({
      name: item.name,
      url: item.html_url,
      gravatarUrl: item.owner.gravatar_url,
      description: item.description
    }));
  }
}
