import { GithubTokenProvider } from './github-token-provider';

describe('GithubTokenProvider', () => {
  let tokenProvider: GithubTokenProvider;

  beforeEach(() => {
    tokenProvider = new GithubTokenProvider();
    sessionStorage.clear();
  });

  it('should get the token from session storage', () => {
    // given
    sessionStorage.setItem('token', 'my token');

    // then
    expect(tokenProvider.token).toBe('my token');
  });
});
