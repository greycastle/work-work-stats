const tokenKey = 'token';

export class GithubTokenProvider {
  get hasToken(): boolean {
    return this.token !== null;
  }

  clearToken(): any {
    sessionStorage.removeItem(tokenKey);
  }

  get token(): string {
      return sessionStorage.getItem(tokenKey);
  }

  set token(value: string) {
    sessionStorage.setItem(tokenKey, value);
  }
}
