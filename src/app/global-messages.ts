
export const UnhandledFailureMessage = 'So sorry! It seems there has been an error. We will look into it. Please try again later.';