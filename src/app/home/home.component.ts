import { AuthService } from './../auth.service';
import { TeamService } from './../team.service';
import { Component, OnInit } from '@angular/core';
import { ProfileService } from '../profile.service';
import { FormControl } from '@angular/forms';
import { GithubTokenProvider } from '../github-token-provider';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  token = new FormControl();
  loading: boolean;
  organisations: any[];
  error: string;
  hasTeam: boolean;
  hasToken: boolean;

  constructor(
    private profileService: ProfileService,
    private teamService: TeamService,
    private tokenProvider: GithubTokenProvider,
    private authService: AuthService) {}

  ngOnInit(): void {
    this.hasTeam = this.teamService.repositories.length > 0;

    this.token.setValue(this.tokenProvider.token);
    this.token.valueChanges.subscribe((value) => {
      this.tokenProvider.token = value;
    });

    this.hasToken = this.token.value && this.token.value.length > 0;

    // automatically fetch the stuff if we already have a token
    if (this.token.value) {
      this.fetchProfile();
    }
  }

  fetchProfile() {
    this.tokenProvider.token = this.token.value;
    this.loading = true;
    this.profileService.getOrganisations(this.token.value)
      .then((orgs) => {
        this.hasToken = true;
        this.error = null;
        this.organisations = orgs;
        this.loading = false;
      })
      .catch(err => {
        this.error = JSON.stringify(err, null, 2);
        this.token.setValue(null);
        this.loading = false;
      });
  }

  login() {
    this.authService.githubLogin()
      .then(token => {
        this.token.setValue(token);
        this.fetchProfile();
      }).catch(err => {
        this.error = 'Login failed.';
        console.log(err);
      });
  }
}
