import { HumanizePipe } from './humanize.pipe';
import { duration } from 'moment';

describe('HumanizePipe', () => {
  it('should format a duration as humanly readable', () => {
    const pipe = new HumanizePipe();
    expect(pipe.transform(duration(3, 'hours'))).toBe('3 hours');
  });
});
