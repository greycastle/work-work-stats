import { Duration } from 'moment';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'humanize'
})
export class HumanizePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    const duration = value as Duration;
    if (duration) {
      return duration.humanize();
    }

    throw new Error(`Unsupported item type: ${value}`);
  }

}
