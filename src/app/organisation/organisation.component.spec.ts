import { OrganisationsService } from './../organisations.service';
import { RepoCardViewModel } from './../repo-card-view-model';
import { RouterTestingModule } from '@angular/router/testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationComponent } from './organisation.component';
import { ActivatedRoute } from '@angular/router';
import { Component, Output, EventEmitter, Input } from '@angular/core';
import { By } from '@angular/platform-browser';
import { TeamService } from '../team.service';

@Component({selector: 'app-search-repository', template: ''})
class StubSearchComponent {
    @Output()
    results: EventEmitter<RepoCardViewModel> = new EventEmitter();

    @Input()
    organisation: string;
}

@Component({selector: 'app-back', template: ''})
class StubBackComponent {}

@Component({selector: 'app-add-team-repo-button', template: ''})
class StubAddToTeamButtonComponent {
    @Input()
    repositoryName: string;
}

describe('OrganisationComponent', () => {
    let fixture: ComponentFixture<OrganisationComponent>;
    let component: OrganisationComponent;

    const activatedRoute: any = {
        snapshot: {
            paramMap: {
                get: (name: string): string => 'greycastle'
            }
        }
    };

    beforeEach(() => {
      const orgService = jasmine.createSpyObj<OrganisationsService>(['get']);
      TestBed.configureTestingModule({
          declarations: [
              OrganisationComponent,
              StubSearchComponent,
              StubBackComponent,
              StubAddToTeamButtonComponent
          ],
          imports: [
              RouterTestingModule
          ],
          providers: [
              { provide: ActivatedRoute, useValue: activatedRoute },
              TeamService,
              { provide: OrganisationsService, useValue: orgService }
          ]
      }).compileComponents();

      fixture = TestBed.createComponent(OrganisationComponent);
      component = fixture.componentInstance;
    });

    it('should know which organisation is being displayed', () => {
        component.ngOnInit();
        expect(component.organisationName).toBe('greycastle');
    });

    it('should load previous search from session', () => {
        // given previous search
        const searchResult = new RepoCardViewModel();
        searchResult.name = 'greycastle';
        searchResult.url = 'url';
        searchResult.description = 'description';
        sessionStorage.setItem('repositorySearch', JSON.stringify([ searchResult ]));

        // when we've loaded
        component.ngOnInit();

        // then we should have the same items
        expect(component.repositories).toEqual([searchResult]);
    });

    it('should silently fail and clear cache if it is invalid', () => {
        // given an invalid cache
        sessionStorage.setItem('repositorySearch', '{} invalid json');

        // when we load
        component.ngOnInit();

        // the cache should have been cleared
        expect(sessionStorage.getItem('repositorySearch')).toBeNull();
    });

    it('should display a list of cards when search comes back with results', () => {
        // given results
        const viewModel = new RepoCardViewModel();
        viewModel.name = 'greycastle';
        viewModel.url = 'https://github.com/greycastle'

        // when they are emitted from search
        fixture.debugElement.query(By.directive(StubSearchComponent))
            .triggerEventHandler('results', [ viewModel ]);

        // then we display them as cards
        fixture.detectChanges();
        const element = fixture.debugElement.query(By.css('.card .header')).nativeElement;
        expect(element.innerText).toBe('greycastle');
    });
});
