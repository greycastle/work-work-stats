import { SearchRepositoryComponent } from './../search-repository/search-repository.component';
import { OrganisationsService } from './../organisations.service';
import { RepoCardViewModel } from './../repo-card-view-model';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { TeamService } from '../team.service';

@Component({
  selector: 'app-organisation',
  templateUrl: './organisation.component.html',
  styleUrls: ['./organisation.component.scss']
})
export class OrganisationComponent implements OnInit {
  private readonly cacheName = 'repositorySearch';

  organisationName: string;
  repositories: RepoCardViewModel[];
  error: string;

  constructor(private route: ActivatedRoute, private teamService: TeamService, private orgService: OrganisationsService) { }

  ngOnInit() {
    this.organisationName = this.route.snapshot.paramMap.get('org');

    const cacheJson = sessionStorage.getItem(this.cacheName);
    if (cacheJson) {
      this.repositories = this.readCache(cacheJson);
    } else {
      this.loadRepos(this.organisationName);
    }

    this.teamService.setOrganisation(this.organisationName);
  }

  async loadRepos(orgName: string) {
    try {
      const repos = await this.orgService.get(orgName);
      const repoCards = this.mapToCards(repos, orgName);
      this.searchCompleted(repoCards);
    } catch (err) {
      console.log(err);
    }
  }

  private mapToCards(repos: any[], orgName: string) {
    return repos.map(repo => RepoCardViewModel.build({
      name: repo.name,
      url: `https://github.com/${orgName}/${repo.name}`,
      description: repo.description,
    }));
  }

  searchCompleted(items: RepoCardViewModel[]) {
    this.repositories = items;
    sessionStorage.setItem(this.cacheName, JSON.stringify(this.repositories));
  }

  readCache(cacheJson: string): RepoCardViewModel[] {
    try {
      const objects = JSON.parse(cacheJson);
      return objects.map((item: any) => {
        const model = new RepoCardViewModel();
        model.name = item.name;
        model.url = item.url;
        model.description = item.description;
        return model;
      });
    } catch (err) {
      console.log(err);
      sessionStorage.removeItem(this.cacheName);
    }
  }
}
