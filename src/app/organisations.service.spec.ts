import { GithubApiClientService } from './github-api-client.service';
import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { OrganisationsService } from './organisations.service';
import { GithubTokenProvider } from './github-token-provider';
import { environment } from 'src/environments/environment.dev';

describe('OrganisationsService', () => {
    let service: OrganisationsService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [ HttpClientModule ],
            providers: [
                GithubApiClientService,
                GithubTokenProvider,
                OrganisationsService
            ]
        });

        const tokenProvider: GithubTokenProvider = TestBed.get(GithubTokenProvider);
        tokenProvider.token = environment.testToken;

        service = TestBed.get(OrganisationsService);
    });

    it('should list repositories for organisation', async () => {
        // given
        const organisation = 'flutter';

        // when
        const repos = await service.get(organisation);

        // then
        const engineRepo = repos.find(r => r.name === 'engine');
        expect(engineRepo).toBeTruthy();
    })
});
