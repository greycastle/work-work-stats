import { GithubApiClientService } from './github-api-client.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class OrganisationsService {
  constructor(private githubApi: GithubApiClientService) { }

  async get(orgName: string): Promise<any[]> {
    const url = `orgs/${orgName}/repos?sort=full_name`;
    const repos = await this.githubApi.get(url).toPromise();
    return this.mapRepos(orgName, repos);
  }

  private mapRepos(orgName: string, repos: any[]): any[] {
    return repos.map(repo => ({
      name: repo.name,
      description: repo.description,
      orgName
    }));
  }
}
