import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ProfileService {
  constructor(private httpClient: HttpClient) { }

  async getOrganisations(token: string): Promise<any> {
    const headers = {
      Accept: 'application/vnd.github.antiope-preview+json',
      Authorization: `token ${token}`
    };

    const orgs = await this.httpClient.get('https://api.github.com/user/orgs', { headers }).toPromise<any>();

    return orgs.map(org => ({
      name: org.login,
      avatar: org.avatar_url,
      description: org.description
    }));
  }
}
