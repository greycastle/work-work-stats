import { HumanizePipe } from './../humanize.pipe';
import { ChangeRatePipe } from './../change-rate.pipe';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PullRequestLifetimeStatsComponent, OldestPullRequestLifetimeStatsComponent } from './pull-request-lifetime-stats.component';
import { WeekByWeek, PRStats } from '../weekly-team-stats';
import { duration } from 'moment';
import { Differences } from '../diff.service';
import { getLocaleDateTimeFormat } from '@angular/common';

describe('PullRequestLifetimeStatsComponentBase', () => {
  let component: PullRequestLifetimeStatsComponent;

  describe('ViewModel', () => {
    beforeEach(() => {
      component = new PullRequestLifetimeStatsComponent();
    });

    it('should show difference as bad if going down', () => {
      component.data = new WeekByWeek(duration(10, 'hours'), duration(14, 'hours'));
      expect(component.difference).toEqual(4);
      expect(component.diffState).toEqual('bad');
    });

    it('should be all green if pull requests are merged within 24h', () => {
      component.data = new WeekByWeek(duration(30, 'hours'), duration(23, 'hours'));
      expect(component.state).toBe('good');
      expect(component.icon).toBe('check icon');
      expect(component.description).toBe('Awesome! PRs are merged within 24h');
    });

    it('should warn if mergin takes more than a day', () => {
      component.data = new WeekByWeek(duration(30, 'hours'), duration(35, 'hours'));
      expect(component.state).toBe('warning');
      expect(component.icon).toBe('warning icon');
      expect(component.description).toBe('This could be better, try to merge PRs under 24h.');
    });

    it('should warn if merge time is longer than three days but if were at least getting better', () => {
      component.data = new WeekByWeek(duration(80, 'hour'), duration(72, 'hours'));
      expect(component.state).toBe('warning');
      expect(component.icon).toBe('thumbs up outline icon');
      expect(component.description).toBe('Looks like PRs are getting merged faster, keep it up!');
    });

    it('should be bad if time is over three days', () => {
      component.data = new WeekByWeek(duration(80, 'hours'), duration(80, 'hours'));

      expect(component.icon).toBe('thumbs down outline icon');
      expect(component.state).toBe('bad');
      expect(component.description).toBe('PRs must be merged faster, otherwise there\'ll be merge hell!');
    });
  });
});

describe('PullRequestLifetimeStatsComponent', () => {
  let component: PullRequestLifetimeStatsComponent;
  let fixture: ComponentFixture<PullRequestLifetimeStatsComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ PullRequestLifetimeStatsComponent, ChangeRatePipe, HumanizePipe ]
    }).compileComponents();

    fixture = TestBed.createComponent(PullRequestLifetimeStatsComponent);
    component = fixture.componentInstance;
  });

  it('should be able to build and bind', () => {
    component.data = new WeekByWeek(duration(5, 'hours'), duration(3, 'hours'));
    fixture.detectChanges();
  });
});

describe('OldestPullRequestLifetimeStatsComponent', () => {
  let component: OldestPullRequestLifetimeStatsComponent;
  let fixture: ComponentFixture<OldestPullRequestLifetimeStatsComponent>;

  beforeEach(async () => {
    TestBed.configureTestingModule({
      declarations: [ OldestPullRequestLifetimeStatsComponent, ChangeRatePipe, HumanizePipe ]
    }).compileComponents();

    fixture = TestBed.createComponent(OldestPullRequestLifetimeStatsComponent);
    component = fixture.componentInstance;
  });

  it('should be able to build and bind', () => {
    const lastWeek: PRStats = {
      changes: new Differences(0, 0),
      closed: new Date(2019, 2, 13, 22, 15),
      opened: new Date(2019, 2, 11, 14, 30),
      author: 'author',
      number: 1,
      repository: 'repo',
      organisation: 'org',
      isMerged: true
    };
    const thisWeek: PRStats = {
      changes: new Differences(0, 0),
      closed: new Date(2019, 2, 21, 10, 15),
      opened: new Date(2019, 2, 20, 11, 30),
      author: 'author',
      number: 1,
      repository: 'repo',
      organisation: 'org',
      isMerged: true
    };
    component.data = new WeekByWeek(lastWeek, thisWeek);
    fixture.detectChanges();
  });
});
