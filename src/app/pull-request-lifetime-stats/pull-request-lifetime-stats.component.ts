import { Component, Input } from '@angular/core';
import { Duration, duration } from 'moment';
import { WeekByWeek, PRStats } from '../weekly-team-stats';
import * as moment from 'moment';

class PullRequestLifetimeStatsComponentBase {

  protected age: WeekByWeek<Duration>;

  @Input()
  title: string;

  get state(): string {
    if (this.age.thisWeek.asDays() > 4) {
      return 'bad';
    } else if (this.age.thisWeek.asDays() > 3) {
      return this.gettingBetter ? 'warning' : 'bad';
    } else if (this.age.thisWeek.asDays() > 1) {
      return 'warning';
    }

    return 'good';
  }

  get icon(): string {
    if (this.state === 'good') {
      return 'check icon';
    } else if (this.state === 'warning') {
      return this.gettingBetter ? 'thumbs up outline icon' : 'warning icon';
    }

    return 'thumbs down outline icon';
  }

  get gettingBetter(): boolean {
    const changeRate = Math.abs(this.difference / this.age.thisWeek.asHours());

    // if we're actually decreasing in time and it's decreasing more than 10%
    return this.difference < 0 && changeRate > 0.1;
  }

  get description(): string {
    if (this.state === 'warning') {
      return this.gettingBetter
        ? 'Looks like PRs are getting merged faster, keep it up!'
        : 'This could be better, try to merge PRs under 24h.';
    } else if (this.state === 'bad') {
      return 'PRs must be merged faster, otherwise there\'ll be merge hell!';
    }

    return 'Awesome! PRs are merged within 24h';
  }

  get difference(): number {
    return this.age.thisWeek.hours() - this.age.lastWeek.hours();
  }

  get diffState(): string {
    return this.age.thisWeek.hours() > this.age.lastWeek.hours() ? 'bad' : 'good';
  }
}

@Component({
  selector: 'app-pull-request-lifetime-stats',
  templateUrl: './pull-request-lifetime-stats.component.html',
  styleUrls: ['./pull-request-lifetime-stats.component.scss']
})
export class PullRequestLifetimeStatsComponent extends PullRequestLifetimeStatsComponentBase {
  @Input()
  data: WeekByWeek<Duration>;

  protected get age(): WeekByWeek<Duration> {
    return this.data;
  }
}

function getPullRequestAge(pr: PRStats): Duration {
  const closeDate = pr.isMerged ? pr.closed : new Date();
  return duration(moment(closeDate).diff(pr.opened));
}

@Component({
  selector: 'app-oldest-pull-request-lifetime-stats',
  templateUrl: './pull-request-lifetime-stats.component.html',
  styleUrls: ['./pull-request-lifetime-stats.component.scss']
})
export class OldestPullRequestLifetimeStatsComponent extends PullRequestLifetimeStatsComponentBase {
  @Input()
  data: WeekByWeek<PRStats>;

  protected get age(): WeekByWeek<Duration> {
    return new WeekByWeek(
      getPullRequestAge(this.data.lastWeek),
      getPullRequestAge(this.data.thisWeek)
    );
  }
}
