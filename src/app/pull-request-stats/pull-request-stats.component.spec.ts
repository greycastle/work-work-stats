import { ChangeRatePipe } from '../change-rate.pipe';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { PullRequestStatsComponent } from './pull-request-stats.component';
import { WeekByWeek } from '../weekly-team-stats';
import { Differences } from '../diff.service';

describe('LargestPullRequestComponent', () => {
  let component: PullRequestStatsComponent;

  describe('ComponentHtml', () => {
    let fixture: ComponentFixture<PullRequestStatsComponent>;

    beforeEach(() => {
      TestBed.configureTestingModule({
        declarations: [ PullRequestStatsComponent, ChangeRatePipe ]
      }).compileComponents();

      fixture = TestBed.createComponent(PullRequestStatsComponent);
      component = fixture.componentInstance;
    });

    it('should be able to build and bind', () => {
      component.data = new WeekByWeek(new Differences(10, 20), new Differences(50, 10));
      fixture.detectChanges();
    });
  });

  describe('ViewModel', () => {
    beforeEach(() => {
      component = new PullRequestStatsComponent();
    });

    it('should calculate differences from last week', () => {
      component.data = new WeekByWeek(new Differences(20, 40), new Differences(40, 80));
      expect(component.comparison).toEqual(new Differences(20, 40));
    });

    it('should warn if differences are going bad', () => {
      component.data = new WeekByWeek(new Differences(250, 25), new Differences(350, 235));
      expect(component.additions).toBe('warning');
      expect(component.deletions).toBe('bad');
    });

    it('should flag differences in week if they are good', () => {
      component.data = new WeekByWeek(new Differences(500, 100), new Differences(220, 70));
      expect(component.additions).toBe('good');
      expect(component.deletions).toBe('good');
    });

    it('should be red flagged if changes are over 500', () => {
      component.data = new WeekByWeek(new Differences(0, 0), new Differences(501, 0));

      expect(component.icon).toBe('thumbs down outline icon');
      expect(component.state).toBe('bad');
      expect(component.description).toBe('Wow! Time for change. Keep PRs small!');
    });

    it('should be warning flagged if changes are over 300', () => {
      component.data = new WeekByWeek(new Differences(0, 0), new Differences(300, 20));

      expect(component.icon).toBe('warning icon');
      expect(component.state).toBe('warning');
      expect(component.description).toBe('This could be better, try to keep PRs under 300 lines of change.');
    });

    it('should be good flagged if changes are below 300', () => {
      component.data = new WeekByWeek(new Differences(0, 0), new Differences(150, 50));

      expect(component.icon).toBe('check icon');
      expect(component.state).toBe('good');
      expect(component.description).toBe('Looking good! Keep PR change size below 300.');
    });

    it('should be warning if state is bad but is getting better', () => {
      component.data = new WeekByWeek(new Differences(1900, 400), new Differences(950, 200));

      expect(component.icon).toBe('thumbs up outline icon');
      expect(component.state).toBe('warning');
      expect(component.description).toBe('PRs are too big but things are getting better, keep it up!');
    });
  });
});
