import { Component, Input } from '@angular/core';
import { Differences } from '../diff.service';
import { WeekByWeek } from '../weekly-team-stats';

@Component({
  selector: 'app-pull-request-stats',
  templateUrl: './pull-request-stats.component.html',
  styleUrls: ['./pull-request-stats.component.scss']
})
export class PullRequestStatsComponent {
  @Input()
  data: WeekByWeek<Differences>;

  @Input()
  title: string;

  get state(): string {
    if (this.data.thisWeek.total === 0) {
      return 'unknown';
    } else if (this.data.thisWeek.total > 500) {
      return this.gettingBetter ? 'warning' : 'bad';
    } else if (this.data.thisWeek.total > 300) {
      return 'warning';
    }

    return 'good';
  }

  get icon(): string {
    if (this.state === 'good') {
      return 'check icon';
    } else if (this.state === 'warning') {
      return this.gettingBetter ? 'thumbs up outline icon' : 'warning icon';
    }

    return 'thumbs down outline icon';
  }

  get gettingBetter(): boolean {
    const change = this.comparison.total;
    const changeRate = Math.abs(change / this.data.thisWeek.total);

    // if we're seeing a negative trend in PR size and the change is more than 5%
    return change < 0 && changeRate > 0.05;
  }

  get description(): string {
    if (this.state === 'good') {
      return 'Looking good! Keep PR change size below 300.';
    } else if (this.state === 'warning') {
      return this.gettingBetter
        ? 'PRs are too big but things are getting better, keep it up!'
        : 'This could be better, try to keep PRs under 300 lines of change.';
    }

    return 'Wow! Time for change. Keep PRs small!';
  }

  get comparison(): Differences {
    return new Differences(
      this.data.thisWeek.additions - this.data.lastWeek.additions,
      this.data.thisWeek.deletions - this.data.lastWeek.deletions);
  }

  private changeRate(thisWeek: number, lastWeek: number): string {
    if (this.data.thisWeek.total < 300) {
      return 'good';
    }

    const diff = thisWeek - lastWeek;
    if (diff <= 0) {
      return 'good';
    }

    if (thisWeek > 500 || diff > 100) {
      return 'bad';
    }

    return 'warning';
  }

  get deletions(): string {
    return this.changeRate(this.data.thisWeek.deletions, this.data.lastWeek.deletions);
  }

  get additions(): string {
    return this.changeRate(this.data.thisWeek.additions, this.data.lastWeek.additions);
  }
}
