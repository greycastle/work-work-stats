import { GithubTokenProvider } from './github-token-provider';
import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';

import { PullRequestService } from './pull-request.service';
import { environment } from 'src/environments/environment.dev';
import { GithubApiClientService } from './github-api-client.service';

describe('PullRequestService', () => {
  let service: PullRequestService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [
        GithubTokenProvider,
        GithubApiClientService
      ]
    });

    const tokenProvider: GithubTokenProvider = TestBed.get(GithubTokenProvider);
    tokenProvider.token = environment.testToken;

    service = TestBed.get(PullRequestService);
  });

  it('should return pull requests within time span', async () => {
    // given two dates
    const start = new Date(2019, 2, 1);
    const end = new Date(2019, 2, 30);

    // when getting pull requests
    const prs = await service.get(start, end, 'SeleniumHQ', 'selenium');
    const numbers = prs.map(pr => pr.number);

    // contain a pr that was closed during the period
    expect(numbers).toContain(7065);

    // and one that's opened during the period
    expect(numbers).toContain(6989);

    // and should not contain any pr closed before the given time
    const closedBefore = prs.filter(pr => pr.closed !== null && pr.closed < start);
    expect(closedBefore).toEqual([]);

    // and should not contain any pr opened after the period
    const openedAfter = prs.filter(pr => pr.opened > end);
    expect(openedAfter).toEqual([]);
  });
});
