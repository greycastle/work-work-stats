import { GithubApiClientService } from './github-api-client.service';
import { Injectable } from '@angular/core';
import { PullRequest } from './weekly-team-stats';
import * as moment from 'moment';

/**
 * This class only handles one page of results now, it'll have to be extended if noticing that more PRs occur.
 */
@Injectable({
  providedIn: 'root'
})
export class PullRequestService {
  constructor(private apiClient: GithubApiClientService) { }

  async get(start: Date, end: Date, organisation: string, repository: string): Promise<PullRequest[]> {
    const createdBefore = moment(end).add(1, 'day').format('YYYY-MM-DD');
    const url = `search/issues?q=is:pr+created:<${createdBefore}+repo:${organisation}/${repository}&sort=created&order=desc`;

    return new Promise((resolve, reject) => {
      this.apiClient.get(url)
        .subscribe(response => {
          const prs = this.map(response.items, organisation, repository);
          resolve(prs.filter(pr => this.inInterval(start, end, pr)));
        }, err => reject(err));
    });
  }

  private inInterval(start: Date, end: Date, pr: PullRequest): boolean {
    // it was closed before start
    if (pr.closed !== null && pr.closed < start) {
      return false;
    }

    // if it was opened after period
    if (pr.opened > end) {
      return false;
    }

    return true;
  }

  private map(items: any[], organisation: string, repository: string): PullRequest[] {
    return items.map(item => ({
      number: item.number,
      repository,
      organisation,
      author: item.user.login,
      opened: this.asDate(item.created_at),
      isMerged: item.closed_at !== null,
      closed: this.asDate(item.closed_at)
    }));
  }

  private asDate(date: any): Date {
    if (date) {
      return moment(date).toDate();
    }

    return null;
  }
}
