export class RepoCardViewModel {
    name: string;
    url: string;
    description: string;

    static build(options: {
        name: string,
        url?: string | undefined,
        description?: string | undefined
    }): RepoCardViewModel {
        const model = new RepoCardViewModel();
        model.name = options.name;
        model.url = options.url || '';
        model.description = options.description || '';
        return model;
    }
}
