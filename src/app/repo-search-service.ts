import { Observable } from 'rxjs';
import { RepositorySearchResultEntry } from './repository-search-result-entry';
import { InjectionToken } from '@angular/core';

export interface RepoSearchService {
    search(organisation: string, filter: string): Observable<RepositorySearchResultEntry[]>;
}

export const RepoSearchServiceToken = new InjectionToken<RepoSearchService>('RepoSearchService');