export class RepositorySearchResultEntry {
    gravatarUrl: string;
    htmlUrl: string;
    name: string;
    url: any;
    description: string;

    constructor(name: string) {
        this.name = name;
    }

    static build(properties: {
        name: string,
        url?: string,
        gravatarUrl?: string,
        description?: string
    }) {
        const entry = new RepositorySearchResultEntry(properties.name);
        entry.url = properties.url || '';
        entry.gravatarUrl = properties.gravatarUrl || '';
        entry.description = properties.description || '';
        return entry;
    }
}
