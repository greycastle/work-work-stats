import { Injectable } from '@angular/core';
import * as moment from 'moment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RepositoryService {
  constructor(private httpClient: HttpClient) { }

  private async getPRStats(pulls: any[]) {
    const merged = pulls.filter(pull => pull.merged_at !== null || pull.state === 'open');
    merged.sort((pull: any, pull2: any) => moment(pull.created_at).diff(pull2.created_at));

    const results = [];
    for (const pull of merged) {
      const closeTimeOrNow = pull.state === 'open' ? moment() : moment(pull.merged_at);
      const openFor = moment.duration(closeTimeOrNow.diff(moment(pull.created_at))).humanize();

      results.push({
        url: pull.html_url,
        author: pull.user.login,
        branchLife: '',
        title: pull.title,
        merged_at: pull.merged_at,
        open_for: openFor,
        state: pull.state
      });
    }

    return results;
}

  private getHeaders(): any {
    const token = sessionStorage.getItem('token');
    return {
      Accept: 'application/vnd.github.antiope-preview+json',
      Authorization: `token ${token}`
    };
  }

  async get(org: string, repo: string): Promise<any> {
    const pulls = await this.httpClient.get(`https://api.github.com/repos/${org}/${repo}/pulls?state=all`,
      { headers: this.getHeaders() }).toPromise<any>();

    return await this.getPRStats(pulls);
  }
}
