import { TeamService } from './../team.service';
import { ContributorStatsService } from './../contributor-stats.service';
import { Component, OnInit } from '@angular/core';
import { RepositoryService } from '../repository.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-repository',
  templateUrl: './repository.component.html',
  styleUrls: ['./repository.component.sass']
})
export class RepositoryComponent implements OnInit {
  branches: any;
  stats: any;
  loading: boolean;
  error: string;

  constructor(private route: ActivatedRoute,
              private contributorStats: ContributorStatsService,
              private teamService: TeamService,
              private repositoryService: RepositoryService) { }

    ngOnInit() {
      const org = this.route.snapshot.paramMap.get('org');
      const repo = this.route.snapshot.paramMap.get('repo');
      this.loading = true;
      this.error = null;
      this.contributorStats.get(org, repo)
        .then(contributorStats => {
          this.stats = contributorStats;
          this.stats.contributors.forEach(c => c.teamMember = this.teamService.isMember(c.name));
          this.repositoryService.get(org, repo)
            .then(stats => {
              this.branches = stats;
              this.loading = false;
            })
            .catch(err => this.handleError(err));
      }).catch(err => this.handleError(err));
    }

  handleError(err: any): void {
    console.log(err);
    this.error = 'An error has occured, this may be temporary so you can try again, for example after clearing cache/local storage.';
  }

  toggleTeamMember(contributor: any) {
    contributor.teamMember = !contributor.teamMember;
    if (contributor.teamMember) {
      this.teamService.addMember(contributor.name);
    } else {
      this.teamService.removeMember(contributor.name);
    }
  }
}
