import { UnhandledFailureMessage } from './../global-messages';
import { RepoCardViewModel } from '../repo-card-view-model';
import { RepoSearchService, RepoSearchServiceToken } from './../repo-search-service';
import { TestBed, ComponentFixture, fakeAsync, tick } from '@angular/core/testing';
import { SearchRepositoryComponent } from './search-repository.component';
import { of } from 'rxjs';
import { RepositorySearchResultEntry } from '../repository-search-result-entry';
import { delay } from 'rxjs/operators';
import { ReactiveFormsModule } from '@angular/forms';

describe('SearchRepositoryComponent', () => {
  const debounceTime = 400;

  let fixture: ComponentFixture<SearchRepositoryComponent>;
  let component: SearchRepositoryComponent;
  let searchService: jasmine.SpyObj<RepoSearchService>;

  beforeEach(() => {
    searchService = jasmine.createSpyObj<RepoSearchService>(['search']);

    TestBed.configureTestingModule({
      declarations: [ SearchRepositoryComponent ],
      imports: [
        ReactiveFormsModule
      ],
      providers: [
        { provide: RepoSearchServiceToken, useValue: searchService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SearchRepositoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  const theSearchReturns = (result: RepositorySearchResultEntry) => {
    searchService.search.and.returnValue(of([result]));
  };

  const theSearchReturnsSomeResponse = () => {
    theSearchReturns(new RepositorySearchResultEntry('example'));
  };

  const theSearchHasCompleted = () => {
    tick(debounceTime);
    fixture.detectChanges();
  };

  it('should have emitted a list of viewmodels after search has completed', fakeAsync(() => {
    // given
    const example = RepositorySearchResultEntry.build({
      name: 'repo name',
      url: 'repo url',
      description: 'description'
    });
    theSearchReturns(example);

    // and we are listening for emitted results
    let results: RepoCardViewModel[];
    component.results.subscribe(emitted => results = emitted);

    // when I search for something
    component.text.setValue('search');

    // and
    theSearchHasCompleted();

    // then I have valid viewmodels
    const expected = new RepoCardViewModel();
    expected.name = example.name;
    expected.url = example.url;
    expected.description = example.description;
    expect(results).toContain(expected);
  }));

  it('it should trigger search after delay if text has changed', fakeAsync(() => {
    // given
    theSearchReturnsSomeResponse(); // and I search
    component.text.setValue('search');

    // when
    theSearchHasCompleted();

    // then
    expect(searchService.search).toHaveBeenCalled();
  }));

  it('it should not trigger search if text changed but changed back within delay', fakeAsync(() => {
    // given I have done one search
    theSearchReturnsSomeResponse(); // and I search
    component.text.setValue('search');
    tick(debounceTime);
    fixture.detectChanges();

    // when I begin writing
    component.text.setValue('search for');

    // but then quickly undo
    tick(50);
    component.text.setValue('search');

    // and
    theSearchHasCompleted();

    // then the service doesn't get called again
    expect(searchService.search).toHaveBeenCalledTimes(1);
  }));

  it('should stop loading after search has returned results', fakeAsync(() => {
    // given I have done one search
    theSearchReturnsSomeResponse(); // and I search
    component.text.setValue('search');

    // when
    theSearchHasCompleted();

    // then
    expect(component.loading).toBeFalsy();
  }));

  it('should be loading after we start search', fakeAsync(() => {
    // given it takes a bit of time for the response to return
    const stream = of([]).pipe(delay(100));
    searchService.search.and.returnValue(stream);

    // when we search
    component.text.setValue('search');
    theSearchHasCompleted();

    // then we should be loading
    expect(component.loading).toBeTruthy();

    // let search finish
    tick(100);
  }));

  it('should stop loading and display error message if service crashes', fakeAsync(() => {
    // given the search service throws an error
    searchService.search.and.throwError('crazy error');

    // when we search
    component.text.setValue('search');
    theSearchHasCompleted();

    // then loading is false!
    expect(component.loading).toBeFalsy();
    expect(component.error).toBe(UnhandledFailureMessage);
  }));

  it('should clear any previous error on new search', fakeAsync(() => {
    // given we've got an error
    component.error = 'Error message';

    // and
    theSearchReturnsSomeResponse();

    // when we search
    component.text.setValue('search');
    theSearchHasCompleted();

    // then the error message is gone!
    expect(component.error).toBeNull();
  }));
});
