import { FormControl } from '@angular/forms';
import { RepoSearchService, RepoSearchServiceToken } from './../repo-search-service';
import { Component, OnInit, Inject, OnDestroy, EventEmitter, Input, Output } from '@angular/core';
import { RepoCardViewModel } from '../repo-card-view-model';
import { switchMap, debounceTime, tap, distinctUntilChanged } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { present } from './search-repository.presenter';
import { UnhandledFailureMessage } from '../global-messages';

@Component({
  selector: 'app-search-repository',
  templateUrl: './search-repository.component.html',
  styleUrls: ['./search-repository.component.sass']
})
export class SearchRepositoryComponent implements OnInit, OnDestroy {
  error: string = null;
  loading: boolean;
  text = new FormControl();

  @Output()
  results: EventEmitter<RepoCardViewModel[]> = new EventEmitter();

  @Input()
  organisation: string;

  private valueChanges: Subscription;

  constructor(@Inject(RepoSearchServiceToken) private repositorySearch: RepoSearchService) { }

  ngOnInit() {
    this.valueChanges = this.text.valueChanges
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        tap(() => this.loading = true),
        switchMap(value => this.repositorySearch.search(this.organisation, value))
      ).subscribe(searchResults => {
        this.loading = false;
        this.error = null;
        this.results.emit(searchResults.map(present));
      }, error => {
        this.loading = false;
        this.error = UnhandledFailureMessage;
        console.log(error);
      });
  }

  ngOnDestroy(): void {
    this.valueChanges.unsubscribe();
  }
}
