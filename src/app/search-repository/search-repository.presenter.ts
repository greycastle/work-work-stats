import { RepositorySearchResultEntry } from '../repository-search-result-entry';
import { RepoCardViewModel } from '../repo-card-view-model';

export function present(input: RepositorySearchResultEntry): RepoCardViewModel {
    const model = new RepoCardViewModel();
    model.name = input.name;
    model.url = input.url;
    model.description = input.description;
    return model;
}
