import { HttpClientModule } from '@angular/common/http';
import { Team } from './team.service';
import { TestBed } from '@angular/core/testing';

import { TeamStatsCollectorService } from './team-stats-collector.service';
import { GithubApiClientService } from './github-api-client.service';
import { GithubTokenProvider } from './github-token-provider';
import { environment } from 'src/environments/environment.dev';
import { DiffService } from './diff.service';
import { RepositoryService } from './repository.service';

describe('TeamStatsCollectorService', () => {
  let service: TeamStatsCollectorService;

  const team = new Team();
  team.organisation = 'SeleniumHQ';
  team.repositories = [ 'selenium' ];

  // I've used some real contributors from the Selenium repo here, kudos for contributing to selenium
  // and kudos for involuntarily contributing to these integration tests :)
  team.members = [ 'virenv', 'JohnChen0', 'Manju26feb', 'singh811', 'twalpole' ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientModule ],
      providers: [
        GithubApiClientService,
        GithubTokenProvider,
        TeamStatsCollectorService,
        DiffService,
        RepositoryService
      ]
    });

    const tokenProvider: GithubTokenProvider = TestBed.get(GithubTokenProvider);
    tokenProvider.token = environment.testToken;

    service = TestBed.get(TeamStatsCollectorService);
  });

  it('can get oldest pr for both that week and the week before', async () => {
    // when getting the statistics for a given date
    const stats = await service.getStatsFor(team, new Date(2019, 1, 26));

    // then pull requests are limited from two weeks before that date
    expect(stats.pullRequestsStats.oldestPR.lastWeek.number).toEqual(6950);
    expect(stats.pullRequestsStats.oldestPR.lastWeek.opened).toEqual(new Date(Date.UTC(2019, 1, 19, 19, 29, 56)));
  });

  it('will filter PRs only from team members', async () => {
    // when getting statistics for a given date
    const stats = await service.getStatsFor(team, new Date(2019, 1, 26));

    // the pr results are all belonging to team members
    expect(team.members).toContain(stats.pullRequestsStats.oldestPR.lastWeek.author);
    expect(team.members).toContain(stats.pullRequestsStats.oldestPR.thisWeek.author);
  });
});
