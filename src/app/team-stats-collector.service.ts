import { DiffService, Differences } from './diff.service';
import { Injectable } from '@angular/core';
import { Team } from './team.service';
import { WeeklyPRStats, WeeklyPRStatsImpl, PRStats, PullRequest } from './weekly-team-stats';
import { WeekInterval } from './week-interval';
import * as moment from 'moment';
import { PullRequestService } from './pull-request.service';

export abstract class TeamStats {
  readonly pullRequestsStats: WeeklyPRStats;
}

class TeamStatsImpl implements TeamStats {
  constructor(private date: Date, private allPullRequests: PRStats[]) {}

  get pullRequestsStats(): WeeklyPRStats {
    return new WeeklyPRStatsImpl(this.date, this.allPullRequests);
  }
}

@Injectable({
  providedIn: 'root'
})
export class TeamStatsCollectorService {
  constructor(private pullRequestDiff: DiffService, private pullRequests: PullRequestService) {}

  async getStatsFor(team: Team, date: Date): Promise<TeamStats> {
    const lastWeek = WeekInterval.byDate(moment(date).add(-1, 'week').toDate());
    const thisWeek = WeekInterval.byDate(date);

    let pullRequests: PullRequest[] = [];
    for (const repository of team.repositories) {
      const repositoryPullRequests = await this.pullRequests.get(lastWeek.start, thisWeek.end, team.organisation, repository);
      pullRequests = pullRequests.concat(repositoryPullRequests);
    }

    // filter non-team members
    pullRequests = pullRequests.filter(pr => team.members.includes(pr.author));

    const prStats: PRStats[] = [];
    for (const pr of pullRequests) {
      const stats  = await this.pullRequestDiff.getDifferences(pr.number, pr.repository, pr.organisation);
      prStats.push({
        changes: stats,
        number: pr.number,
        repository: pr.repository,
        organisation: pr.organisation,
        author: pr.author,
        opened: pr.opened,
        isMerged: pr.isMerged,
        closed: pr.closed
      });
    }

    return Promise.resolve(new TeamStatsImpl(date, prStats));
  }
}
