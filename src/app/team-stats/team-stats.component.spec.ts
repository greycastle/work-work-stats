import { HumanizePipe } from './../humanize.pipe';
import {
  PullRequestLifetimeStatsComponent,
  OldestPullRequestLifetimeStatsComponent
} from './../pull-request-lifetime-stats/pull-request-lifetime-stats.component';
import { ChangeRatePipe } from './../change-rate.pipe';
import { TeamStatsCollectorService, TeamStats } from './../team-stats-collector.service';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamStatsComponent } from './team-stats.component';
import { Component } from '@angular/core';
import { ContributorStatsService } from '../contributor-stats.service';
import { PullRequestStatsComponent } from '../pull-request-stats/pull-request-stats.component';
import { TeamService } from '../team.service';
import { By } from '@angular/platform-browser';

@Component({selector: 'app-back', template: ''})
class StubBackComponent {}

describe('TeamStatsComponent', () => {

  describe('ComponentHtml', () => {
    let component: TeamStatsComponent;
    let fixture: ComponentFixture<TeamStatsComponent>;
    let contributorStatsService: jasmine.SpyObj<ContributorStatsService>;
    let teamStats: jasmine.SpyObj<TeamStatsCollectorService>;
    let teamService: TeamService;

    beforeEach(async(() => {
      contributorStatsService = jasmine.createSpyObj<ContributorStatsService>(['getTotals']);
      teamStats = jasmine.createSpyObj<TeamStatsCollectorService>(['getStatsFor']);
      TestBed.configureTestingModule({
        declarations: [
          TeamStatsComponent,
          StubBackComponent,
          PullRequestStatsComponent,
          PullRequestLifetimeStatsComponent,
          OldestPullRequestLifetimeStatsComponent,
          ChangeRatePipe,
          HumanizePipe
        ],
        providers: [
          { provide: ContributorStatsService, useValue: contributorStatsService },
          { provide: TeamStatsCollectorService, useValue: teamStats }
        ]
      })
      .compileComponents();

      teamService = TestBed.get(TeamService);
    }));

    it('is loading and yet to get an error after initialise', () => {
      teamService.setOrganisation('Greycastle');
      teamService.addRepository('fruitorvegetable');
      teamService.addMember('ddikman');

      fixture = TestBed.createComponent(TeamStatsComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();

      component.ngOnInit();

      expect(component.loading).toBeTruthy();
      expect(component.error).toBeFalsy();
    });
  });

  describe('ViewModel', () => {
    let teamService: TeamService;
    let teamStatsCollector: jasmine.SpyObj<TeamStatsCollectorService>;
    let contributorService: jasmine.SpyObj<ContributorStatsService>;
    let component: TeamStatsComponent;

    beforeEach(() => {
      teamService = new TeamService();
      teamService.reset();

      contributorService = jasmine.createSpyObj<ContributorStatsService>(['getTotals']);
      teamStatsCollector = jasmine.createSpyObj<TeamStatsCollectorService>(['getStatsFor']);

      component = new TeamStatsComponent(teamService, contributorService, teamStatsCollector);
    });

    it('removes repository from team when clicking button', async () => {
      const stubStats: TeamStats = {
        pullRequestsStats: null
      };
      contributorService.getTotals.and.returnValue(Promise.resolve(null));
      teamStatsCollector.getStatsFor.and.returnValue(Promise.resolve(stubStats));

      teamService.addRepository('tokens');
      teamService.addMember('ddikman');

      await component.ngOnInit();

      component.removeRepository('tokens');
      expect(teamService.repositories).toEqual([]);
    });

    it('cannot display view for team without members', async () => {
      teamService.setOrganisation('Greycastle');
      teamService.addRepository('fruitorvegetable');

      await component.ngOnInit();

      expect(component.error).toContain('The current team does not have any assigned members');
    });

    it('cannot display view for team without repositories', async () => {
      teamService.setOrganisation('Greycastle');

      await component.ngOnInit();

      expect(component.error).toContain('lacking repositories');
    });
  });
});
