import { ContributorStatsService } from './../contributor-stats.service';
import { TeamService } from './../team.service';
import { Component, OnInit } from '@angular/core';
import { TeamStatsCollectorService } from '../team-stats-collector.service';
import { WeeklyPRStats } from '../weekly-team-stats';

@Component({
  selector: 'app-team-stats',
  templateUrl: './team-stats.component.html',
  styleUrls: ['./team-stats.component.scss']
})
export class TeamStatsComponent implements OnInit {
  loading: boolean;
  error: any;
  stats: any;
  repositories: string[];
  prStats: WeeklyPRStats;

  constructor(
    private team: TeamService,
    private contributorStats: ContributorStatsService,
    private teamStats: TeamStatsCollectorService) { }

  ngOnInit() {
    this.fetchData();
  }

  async fetchData() {
    this.loading = true;
    this.error = null;

    if (this.team.repositories.length === 0) {
      this.error = 'The current team is lacking repositories, please add one or more.';
      return;
    }

    if (this.team.members.length === 0) {
      this.error = 'The current team does not have any assigned members. Please add some from the repository page.';
      return;
    }

    try {
      const organisation = this.team.organisation;
      if (!organisation) {
        throw new Error('No organisation defined');
      }

      this.repositories = this.team.repositories;
      this.stats = await this.contributorStats.getTotals(organisation, this.team.repositories, this.team.members);

      const weeklyTeamStats = await this.teamStats.getStatsFor(this.team, new Date());
      this.prStats = weeklyTeamStats.pullRequestsStats;

      this.loading = false;
    } catch (err) {
      console.log(err);
      this.loading = false;
      this.error = 'Unabled to fetch stats. This actually happens whilst stats are collected on github, please try again in a few seconds.';
    }
  }

  removeRepository(repository: string) {
    this.team.removeRepository(repository);
    this.repositories = this.team.repositories;
  }
}
