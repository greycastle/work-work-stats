import { Injectable, EventEmitter } from '@angular/core';

export class Team {
  repositories: string[] = [];
  members: string[] = [];
  organisation: string;
}

@Injectable({
  providedIn: 'root'
})
export class TeamService {
  private readonly storageKey = 'team';
  private team: Team;

  readonly updated: EventEmitter<Team> = new EventEmitter();

  constructor() {
    try {
      const json = localStorage.getItem(this.storageKey);
      if (json) {
        this.team = JSON.parse(json);
      } else {
        this.team = new Team();
      }
    } catch (err) {
      console.log(err);
      this.reset();
    }
  }

  private save() {
    localStorage.setItem(this.storageKey, JSON.stringify(this.team));
    this.updated.emit(this.team);
  }

  addRepository(name: string) {
    if (this.team.repositories.find(r => r.toLowerCase() === name.toLowerCase())) {
      return;
    }

    this.team.repositories.push(name);
    this.save();
  }

  setOrganisation(organisationName: string) {
    this.team.organisation = organisationName;
    this.save();
  }

  removeMember(name: string): void {
    this.team.members = this.team.members.filter(m => m.toLowerCase() !== name.toLowerCase());
    this.save();
  }

  removeRepository(repository: string): void {
    this.team.repositories = this.team.repositories.filter(r => r.toLowerCase() !== repository.toLowerCase());
    this.save();
  }

  addMember(name: string): void {
    if (this.isMember(name)) {
      return;
    }
    this.team.members.push(name);
    this.save();
  }

  isMember(name: string): boolean {
    return this.team.members.find(m => m.toLowerCase() === name.toLowerCase()) !== undefined;
  }

  get repositories(): string[] {
    return this.team.repositories;
  }

  get organisation(): string {
    return this.team.organisation;
  }

  get members(): string[] {
    return this.team.members;
  }

  reset(): void {
    this.team = new Team();
    localStorage.removeItem(this.storageKey);
  }
}
