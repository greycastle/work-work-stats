import { Injectable } from '@angular/core';
declare var $: any;

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor() { }

  show(title: string, message: string) {
    $.uiAlert({
      textHead: title,
      text: message,
      bgcolor: '#55a9ee',
      textcolor: '#fff',
      position: 'bottom-right', // top And bottom ||  left / center / right
      icon: 'checkmark box',
      time: 3
    });
  }
}
