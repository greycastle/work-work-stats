import { GithubTokenProvider } from './github-token-provider';
import { TestBed, async, inject } from '@angular/core/testing';

import { TokenGuard } from './token.guard';
import { Router } from '@angular/router';

describe('TokenGuard', () => {
  let guard: TokenGuard;
  let tokenProvider: GithubTokenProvider;
  let router: jasmine.SpyObj<Router>;

  beforeEach(() => {
    tokenProvider = new GithubTokenProvider();
    router = jasmine.createSpyObj<Router>(['navigate']);
    guard = new TokenGuard(tokenProvider, router);
  });

  it('should redirect to home if there is no token', () => {
    // given there's no token
    tokenProvider.clearToken();

    // when
    const result = guard.canActivate(null, null);

    // then
    expect(result).toBeFalsy();
    expect(router.navigate).toHaveBeenCalledWith(['home']);
  });

  it('should accept access if there is a token', () => {
    // given there is a token
    tokenProvider.token = 'token';

    // then
    expect(guard.canActivate(null, null)).toBeTruthy();
  });
});
