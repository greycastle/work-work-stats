import { GithubTokenProvider } from './github-token-provider';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class TokenGuard implements CanActivate {
  constructor(private tokenProvider: GithubTokenProvider, private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    if (this.tokenProvider.hasToken) {
      return true;
    }

    this.router.navigate(['home']);
    return false;
  }
}
