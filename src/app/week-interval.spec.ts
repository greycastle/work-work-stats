import { WeekInterval } from './week-interval';

describe('WeekInterval', () => {
    it('should be the same week as a given date', () => {
        // when given today
        const week = WeekInterval.byDate(new Date());

        // it should be in the current week
        expect(week.includes(new Date())).toBeTruthy();
    });

    it('should start at midnight Monday same week', () => {
        // given a weekday
        const date = new Date(2019, 3, 9, 12, 32, 12);

        // it should start
        expect(WeekInterval.byDate(date).start).toEqual(new Date(2019, 3, 8, 0, 0, 0, 0));
    });

    it('should end before midnight Monday next week', () => {
        // given a weekday
        const date = new Date(2019, 3, 10, 14, 12, 9);

        // it should start
        expect(WeekInterval.byDate(date).end).toEqual(new Date(2019, 3, 14, 23, 59, 59, 999));
    });

    it('should be after a given date', () => {
        // given the sunday before
        const prevSunday = new Date(2019, 3, 7);
        const week = WeekInterval.byDate(new Date(2019, 3, 8));

        // then it should be after
        expect(week.isAfter(prevSunday)).toBeTruthy();
    });

    it('should be before a given date', () => {
        // given the monday after
        const nextMonday = new Date(2019, 3, 8);
        const week = WeekInterval.byDate(new Date(2019, 3, 7));

        // then it should be before
        expect(week.isBefore(nextMonday)).toBeTruthy();
    });

    it('should return previous week', () => {
        // given a week starting on a monday
        const week = WeekInterval.byDate(new Date(2019, 3, 8));

        // then the start of prev week is prev monday
        expect(week.previous.start).toEqual(new Date(2019, 3, 1));
    });
});
