import * as moment from 'moment';

export class WeekInterval {
    readonly start: Date;
    readonly end: Date;

    private constructor(start: Date, end: Date) {
        this.start = start;
        this.end = end;
    }

    static byDate(sampleDay: Date): WeekInterval {
        const start = moment(sampleDay).startOf('isoWeek').toDate();
        const end = moment(start).endOf('isoWeek').toDate();
        return new WeekInterval(start, end);
    }

    get previous(): WeekInterval {
        return WeekInterval.byDate(moment(this.start).add(-1, 'week').toDate());
    }

    includes(date: Date): boolean {
        return date >= this.start && date < this.end;
    }

    isBefore(date: Date) {
        return this.end < date;
    }

    isAfter(date: Date) {
        return this.start > date;
    }
}