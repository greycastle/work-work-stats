import { WeeklyPRStatsImpl, PRStats } from './weekly-team-stats';
import { Differences } from './diff.service';

class MockPRStats implements PRStats {
  number: number;
  repository: string;
  organisation: string;
  author: string;
  changes: Differences;
  opened: Date;
  isMerged: boolean;
  closed: Date;

  constructor(opened: Date, closed: Date | undefined, changes: Differences) {
    this.opened = opened;
    this.closed = closed;
    this.isMerged = closed !== null;
    this.changes = changes;
  }
}

describe('WeeklyPRStatsImpl', () => {
  const mondayPrevWeek = new Date(2019, 3, 8);
  const tuesdayPrevWeek = new Date(2019, 3, 9);
  const tuesdayThisWeek = new Date(2019, 3, 16);
  const wednesdayThisWeek = new Date(2019, 3, 17);

  it('should compare this weeks largest pr with last weeks', () => {
    // given one pr from last week
    const prs: PRStats[] = [
      new MockPRStats(mondayPrevWeek, tuesdayPrevWeek, new Differences(200, 10)),
    ];

    // and two from this week
    prs.push(new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(400, 80)));
    const largestThisWeek = new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(500, 120));
    prs.push(largestThisWeek);

    // when getting the largest pr week by week
    const stats = new WeeklyPRStatsImpl(wednesdayThisWeek, prs);
    const largestPr = stats.largestPR;

    // then the largest of the pull request is compared
    expect(largestPr.thisWeek).toEqual(largestThisWeek.changes);
  });

  it('should consider last weeks largest pr as without changes if no data exists', () => {
    // given we don't have any pr last week
    const prs: PRStats[] = [
      new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(20, 20))
    ];

    // when getting largest pr
    const largestPr = new WeeklyPRStatsImpl(wednesdayThisWeek, prs).largestPR;

    // then last weeks is considered empty
    expect(largestPr.lastWeek).toEqual(new Differences(0, 0));
  });

  it('should average pr by week', () => {
    // given two prs in a week
    const prs: PRStats[] = [
      new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(250, 100)),
      new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(451, 50)),
    ];

    // then the average is calculated
    const stats = new WeeklyPRStatsImpl(wednesdayThisWeek, prs);
    expect(stats.averagePR.thisWeek).toEqual(new Differences(351, 75));
  });

  it('should count the stats date as the closed date when calculating oldest pr', () => {
    // given an open pr thats older if counting it as closed by the date we count the stats
    const closedPr = new MockPRStats(mondayPrevWeek, tuesdayThisWeek, new Differences(250, 100));
    const openPr = new MockPRStats(mondayPrevWeek, null, new Differences(450, 50));

    // then the open one is oldest
    const stats = new WeeklyPRStatsImpl(wednesdayThisWeek, [ closedPr, openPr ]);
    expect(stats.oldestPR.thisWeek).toEqual(openPr);
  });

  it('should return null if there are no prs to count lifetime for', () => {
    // given that there are no pull requests
    const prs: PRStats[] = [];

    // then the oldest pr is null of either week
    const stats = new WeeklyPRStatsImpl(wednesdayThisWeek, prs);
    expect(stats.oldestPR.thisWeek).toBeNull();
    expect(stats.oldestPR.lastWeek).toBeNull();
  });

  it('should average lifetime of pull requests including open ones', () => {
    // given
    const prs: PRStats[] = [
      new MockPRStats(tuesdayThisWeek, wednesdayThisWeek, new Differences(0, 0)),
      new MockPRStats(tuesdayThisWeek, null, new Differences(0, 0)),
    ];

    // then the average lifetime is between the two
    const stats = new WeeklyPRStatsImpl(wednesdayThisWeek, prs);
    expect(stats.avgPRLifetime.thisWeek.humanize()).toBe('a day');
  });
});

