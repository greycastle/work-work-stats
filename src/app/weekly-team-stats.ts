import { Differences } from './diff.service';
import * as moment from 'moment';
import { WeekInterval } from './week-interval';
import { Duration } from 'moment';

export class WeekByWeek<T> {
    readonly thisWeek: T;
    readonly lastWeek: T;

    constructor(lastWeek: T, thisWeek: T) {
        this.thisWeek = thisWeek;
        this.lastWeek = lastWeek;
    }
}

export class ContributorStats {
    contributor: string;
    total: Differences;
    noCommits: number;
    noPullRequests: number;
}

export abstract class WeeklyPRStats {
    readonly largestPR: WeekByWeek<Differences>;
    readonly averagePR: WeekByWeek<Differences>;
    readonly oldestPR: WeekByWeek<PRStats>;
    readonly avgPRLifetime: WeekByWeek<Duration>;
}

export abstract class PullRequest {
    readonly number: number;
    readonly repository: string;
    readonly organisation: string;
    readonly author: string;
    readonly opened: Date;
    readonly isMerged: boolean;
    readonly closed: Date;
}

export abstract class PRStats extends PullRequest {
    readonly changes: Differences;
}

export class WeeklyPRStatsImpl implements WeeklyPRStats {
    constructor(private atDate: Date, private pullRequests: PRStats[]) {}

    private wasOpenDuringWeek(pr: PRStats, week: WeekInterval) {
        // was opened or closed that week
        if (week.includes(pr.opened) || (pr.isMerged && week.includes(pr.closed))) {
            return true;
        }

        // if was opened and closed before that week
        if (week.isAfter(pr.opened) && (pr.isMerged && week.isBefore(pr.closed))) {
            return true;
        }

        // or it's still open and it was opened before the end of the week
        return !pr.isMerged && week.isAfter(pr.opened);
    }

    private getPullRequestsDuring(week: WeekInterval): PRStats[] {
        return this.pullRequests.filter(pr => this.wasOpenDuringWeek(pr, week));
    }

    private getLargest(stats: Differences[]): Differences {
        return stats.reduce((largest, curr) => curr.total > largest.total ? curr : largest, new Differences(0, 0));
    }

    get largestPR(): WeekByWeek<Differences> {
        const week = WeekInterval.byDate(this.atDate);
        const lastWeek = this.getLargest(this.getPullRequestsDuring(week.previous).map(pr => pr.changes));
        const thisWeek = this.getLargest(this.getPullRequestsDuring(week).map(pr => pr.changes));
        return new WeekByWeek<Differences>(lastWeek, thisWeek);
    }

    private getAverage(stats: Differences[]): Differences {
        if (stats.length === 0) {
            return new Differences(0, 0);
        }

        const additions = stats.reduce((total, pr) => total + pr.additions, 0) / stats.length;
        const deletions = stats.reduce((total, pr) => total + pr.deletions, 0) / stats.length;
        return new Differences(Math.round(additions), Math.round(deletions));
    }

    get averagePR(): WeekByWeek<Differences> {
        const week = WeekInterval.byDate(this.atDate);
        const lastWeek = this.getAverage(this.getPullRequestsDuring(week.previous).map(pr => pr.changes));
        const thisWeek = this.getAverage(this.getPullRequestsDuring(week).map(pr => pr.changes));
        return new WeekByWeek<Differences>(lastWeek, thisWeek);
    }

    private lifetime(pr: PRStats): number {
        return pr.isMerged
        ? moment(pr.closed).diff(moment(pr.opened))
        : moment(this.atDate).diff(moment(pr.opened));
    }

    private getOldest(stats: PRStats[]): PRStats {
        if (stats.length === 0) {
            return null;
        }

        return stats.reduce((oldest, curr) => this.lifetime(curr) > this.lifetime(oldest) ? curr : oldest, stats[0]);
    }

    get oldestPR(): WeekByWeek<PRStats> {
        const week = WeekInterval.byDate(this.atDate);
        const lastWeek = this.getOldest(this.getPullRequestsDuring(week.previous));
        const thisWeek = this.getOldest(this.getPullRequestsDuring(week));
        return new WeekByWeek(lastWeek, thisWeek);
    }

    private getAverageLifetime(stats: PRStats[]): Duration {
        if (stats.length === 0) {
            return null;
        }

        const totalLifetime = stats.reduce((total, curr) => total + this.lifetime(curr), 0);
        return moment.duration(totalLifetime / stats.length);
    }

    get avgPRLifetime(): WeekByWeek<Duration> {
        const week = WeekInterval.byDate(this.atDate);
        const lastWeek = this.getAverageLifetime(this.getPullRequestsDuring(week.previous));
        const thisWeek = this.getAverageLifetime(this.getPullRequestsDuring(week));
        return new WeekByWeek(lastWeek, thisWeek);
    }
}
